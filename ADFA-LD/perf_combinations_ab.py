#!/usr/bin/env python
from __future__ import division
import sys
import re
from libsvm.python import svm
from libsvm.python import svmutil
from itertools import combinations

NU = [0.5, 0.2, 0.1, 0.05, 0.01, 0.005, 0.001, 0.0005, 0.0001]
SET_MIN = 1

def read_data_file(file_path):
    data_file = open(file_path, 'r')
    data_list = list()
    # assume each line is like '+1 1:2.23234 \n'
    re_str = '.*\d\:(.*)\ \n'
    for line in data_file:
        data_list.append(float(re.search(re_str, line).group(1)))

    return data_list


def concat_data(combo_tuple, data_dict):
    res_list = list()
    for i in range(0, len(data_dict[SET_MIN])):
        cur_list = list()
        for value in combo_tuple:
            cur_list.append(data_dict[value][i])
        res_list.append(cur_list)

    return res_list


if __name__ == '__main__':
    try:
        set_min = int(sys.argv[1])
        set_max = int(sys.argv[2]) + 1
        nr_combo = int(sys.argv[3])
    except ValueError:
        print("get min/max failed!")
        sys.exit(1)

    res_file = open('./perf_svm_abs'+str(set_min)+'_s'+str(set_max-1)+'_c'+str(nr_combo)+'.csv', 'w')
    SET_MIN = set_min
    # get single set evaluation results
    train_single = dict()
    val_single = dict()
    attack_single = dict()

    for i in range(set_min, set_max):
        # assume each set is named like 'set#'
        cur_dir = "./set" + str(i)
        train_single[i] = read_data_file(cur_dir+'/svm_train_multi.txt')
        val_single[i] = read_data_file(cur_dir+'/svm_validation_multi.txt')
        attack_single[i] = read_data_file(cur_dir+'/svm_attack_multi.txt')

    combo = list(combinations([i for i in range(set_min, set_max)], nr_combo))
    label_train = [1 for _ in range(0, len(train_single[set_min]))]
    label_val = [1 for _ in range(0, len(val_single[set_min]))]
    label_attack = [-1 for _ in range(0, len(attack_single[set_min]))]

    for item in combo:
        # concat to get SVM input data
        train_data = concat_data(item, train_single)
        val_data = concat_data(item, val_single)
        attack_data = concat_data(item, attack_single)
        # call libsvm
        svm_problem = svm.svm_problem(label_train, train_data)
        res_file.write('-'.join(str(i) for i in item)+',')
        for nu in NU:
            svm_param = svm.svm_parameter('-s 2 -n '+str(nu))
            model = svmutil.svm_train(svm_problem, svm_param)

            # get false postives (as we detect abnormal)
            label, acc, val = svmutil.svm_predict(label_val, val_data, model)
            # tp = int(acc[0] * len(label) / 100)
            # fp = len(label) - int(acc[0] * len(label) / 100)
            fp = 100 - acc[0]
            # get true positives
            label, acc, val = svmutil.svm_predict(label_attack, attack_data, model)
            # tn = int(acc[0] * len(label) / 100)
            # tp = int(acc[0] * len(label) / 100)
            tp = acc[0]
            # res_file.write('{},{},'.format(tp, tn))
            res_file.write('{},{},'.format(fp, tp))
        res_file.write('\n')

    # for i in range(set_min, set_max):
    #     cur_dir = "./set"+str(i)
    #     res_file.write('{},'.format(i))
    #     tl, td = svmutil.svm_read_problem(cur_dir+'/svm_train_multi.txt')
    #     vl, vd = svmutil.svm_read_problem(cur_dir+'/svm_validation_multi.txt')
    #     al, ad = svmutil.svm_read_problem(cur_dir+'/svm_attack_multi.txt')
    #     for nu in NU:
    #         model = svmutil.svm_train(tl, td, '-s 2 -n '+str(nu))
    #         # get true postives
    #         label, acc, val = svmutil.svm_predict(vl, vd, model)
    #         tp = int(acc[0] * len(label) / 100)
    #         # get true negtives
    #         label, acc, val = svmutil.svm_predict(al, ad, model)
    #         tn = int(acc[0] * len(label) / 100)
    #         res_file.write('{},{},'.format(tp, tn))
    #     res_file.write('\n')
    res_file.close()
