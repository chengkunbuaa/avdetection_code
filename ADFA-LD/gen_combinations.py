#!/usr/bin/env python
from __future__ import division
import sys
import re
import ast
from libsvm.python import svm
from libsvm.python import svmutil
from itertools import combinations

NU = [0.5, 0.2, 0.1, 0.05, 0.01, 0.005, 0.001, 0.0005, 0.0001]


def read_data_file(file_path):
    label, data = svmutil.svm_read_problem(file_path)
    return [val[1] for val in data]


def concat_data(combo_tuple, data_dict):
    res_list = list()
    for i in range(0, len(data_dict[1])):
        cur_list = list()
        for value in combo_tuple:
            cur_list.append(data_dict[value][i])
        res_list.append(cur_list)

    return res_list


if __name__ == '__main__':
    try:
        sets = ast.literal_eval('[' + sys.argv[1] + ']')
    except SyntaxError:
        print("get sets failed!")
        sys.exit(1)

    # res_file = open('./perf_svm_s'+str(set_min)+'_s'+str(set_max-1)+'_c'+str(nr_combo)+'.csv', 'w')

    # get single set evaluation results
    train_single = list()
    val_single = list()
    attack_single = list()

    for idx, item in enumerate(sets):
        # assume each set is named like 'set#'
        cur_dir = "./set" + str(item)
        train_single.append(read_data_file(cur_dir+'/svm_train_multi.txt'))
        val_single.append(read_data_file(cur_dir+'/svm_validation_multi.txt'))
        attack_single.append(read_data_file(cur_dir+'/svm_attack_multi.txt'))

    train_data = zip(*train_single)
    val_data = zip(*val_single)
    attack_data = zip(*attack_single)

    train_file = open('./svm_train_'+'_'.join(str(i) for i in sets)+'.txt', 'w')
    for item in train_data:
        train_file.write('+1 ')
        for idx, value in enumerate(item):
            train_file.write('{}:{} '.format(idx+1, value))
        train_file.write('\n')
    train_file.close()

    val_file = open('./svm_validation_' + '_'.join(str(i) for i in sets)+'.txt', 'w')
    for item in val_data:
        val_file.write('+1 ')
        for idx, value in enumerate(item):
            val_file.write('{}:{} '.format(idx + 1, value))
        val_file.write('\n')
    val_file.close()

    attack_file = open('./svm_attack_' + '_'.join(str(i) for i in sets)+'.txt', 'w')
    for item in attack_data:
        attack_file.write('-1 ')
        for idx, value in enumerate(item):
            attack_file.write('{}:{} '.format(idx + 1, value))
        attack_file.write('\n')
    attack_file.close()

    test_file = open('./svm_test_' + '_'.join(str(i) for i in sets)+'.txt', 'w')
    for item in val_data:
        test_file.write('+1 ')
        for idx, value in enumerate(item):
            test_file.write('{}:{} '.format(idx + 1, value))
        test_file.write('\n')
    for item in attack_data:
        test_file.write('-1 ')
        for idx, value in enumerate(item):
            test_file.write('{}:{} '.format(idx + 1, value))
        test_file.write('\n')
    test_file.close()

