#!/bin/bash
set -e
# 1. extracting patterns
for i in `seq 1 20`
do
    { time ./all_grams.py training_compact ${i}; } 2> time_pattern$i.txt
    mkdir set$i
    mv all*.out set$i
    echo "$i. extracting ${i}grams" >> time_patterns.log
    cat time_pattern$i.txt >> time_patterns.log
done



# 2. feature analysis
for i in `seq 1 20`
do
    { time ./prepare_svm_data.sh set${i}; } 2> time_feature$i.txt
    mv svm*.txt set$i
    echo "$i. extacting ${i}gram features" >> time_features.log
    cat time_feature$i.txt >> time_features.log
done
