#!/bin/bash
set -e
for i in `seq 1 20`
do
    { time ./prepare_svm_data_p.sh set${i} ${i}; } 2> time_feature_p${i}.txt &
    # mv svm*${i}*.txt set$i
done

wait

for i in `seq 1 20`
do
    mv svm*$i.txt set$i
done
