#!/usr/bin/env python
from __future__ import division
import sys
import re
import ast
from libsvm.python import svm
from libsvm.python import svmutil

NU = [0.5, 0.2, 0.1, 0.05, 0.01, 0.005, 0.001, 0.0005, 0.0001]


def read_data_file(file_path):
    data_file = open(file_path, 'r')
    data_list = list()
    # assume each line is like '+1 1:2.23234 \n'
    re_str = '.*\d\:(.*)\ \n'
    for line in data_file:
        data_list.append(float(re.search(re_str, line).group(1)))

    return data_list


def concat_data(combo_tuple, data_dict):
    res_list = list()
    for i in range(0, len(data_dict[1])):
        cur_list = list()
        for value in combo_tuple:
            cur_list.append(data_dict[value][i])
        res_list.append(cur_list)

    return res_list


def merge_res(olist, nlist):
    assert(len(olist) == len(nlist))
    return [x+y for x, y in zip(olist, nlist)]


if __name__ == '__main__':
    try:
        m_sets = ast.literal_eval('['+sys.argv[1]+']')
    except SyntaxError:
        print("get candidate set failed!")
        sys.exit(1)

    res_file = open('./perf_svm_mvoter_ab_'+'_'.join(str(i) for i in m_sets)+'.csv', 'w')

    # get single set evaluation results
    train_single = dict()
    val_single = dict()
    attack_single = dict()

    # label_train = [1 for _ in range(0, 833)]
    # label_val = [1 for _ in range(0, 4373)]
    # label_attack = [-1 for _ in range(0, 746)]

    for i in m_sets:
        # assume each set is named like 'set#'
        cur_dir = "./set" + str(i)
        label_train, train_single[i] = svmutil.svm_read_problem(cur_dir+'/svm_train_multi.txt')
        label_val, val_single[i] = svmutil.svm_read_problem(cur_dir+'/svm_validation_multi.txt')
        label_attack, attack_single[i] = svmutil.svm_read_problem(cur_dir+'/svm_attack_multi.txt')

    label_train = [-1 for _ in range(0, len(label_train))]
    label_val = [-1 for _ in range(0, len(label_val))]
    label_attack = [1 for _ in range(0, len(label_attack))]
    res_file.write('-'.join(str(i) for i in m_sets) + ',')
    vlist = [0 for _ in range(0, len(label_val))]
    alist = [0 for _ in range(0, len(label_attack))]
    for nu in NU:
        vlabel = dict()
        alabel = dict()
        for i in m_sets:
            # call libsvm
            svm_problem = svm.svm_problem(label_train, train_single[i])
            svm_param = svm.svm_parameter('-s 2 -n '+str(nu))
            model = svmutil.svm_train(svm_problem, svm_param)
            # get true postives
            vlabel[i], acc, val = svmutil.svm_predict(label_val, val_single[i], model)
            vlist = merge_res(vlist, vlabel[i])
            # get true negtives
            alabel[i], acc, val = svmutil.svm_predict(label_attack, attack_single[i], model)
            alist = merge_res(alist, alabel[i])

        # abnormal is 'postive'
        fp = 0
        for vitem in vlist:
            # find all wrongly labelled as attack (-1)
            if vitem <= 0:
                fp += 1
        tp = 0
        for aitem in alist:
            # find all correctly labelled as attack (-1)
            if aitem <= 0:
                tp += 1
        res_file.write('{0:.2f},{1:.2f},'.format(fp/len(label_val), tp/len(label_attack)))
    res_file.write('\n')

    # for i in range(set_min, set_max):
    #     cur_dir = "./set"+str(i)
    #     res_file.write('{},'.format(i))
    #     tl, td = svmutil.svm_read_problem(cur_dir+'/svm_train_multi.txt')
    #     vl, vd = svmutil.svm_read_problem(cur_dir+'/svm_validation_multi.txt')
    #     al, ad = svmutil.svm_read_problem(cur_dir+'/svm_attack_multi.txt')
    #     for nu in NU:
    #         model = svmutil.svm_train(tl, td, '-s 2 -n '+str(nu))
    #         # get true postives
    #         label, acc, val = svmutil.svm_predict(vl, vd, model)
    #         tp = int(acc[0] * len(label) / 100)
    #         # get true negtives
    #         label, acc, val = svmutil.svm_predict(al, ad, model)
    #         tn = int(acc[0] * len(label) / 100)
    #         res_file.write('{},{},'.format(tp, tn))
    #     res_file.write('\n')
    res_file.close()
