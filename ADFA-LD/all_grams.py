#!/usr/bin/env python3
# import numpy as np
from collections import Counter
import os
import sys
# import ast


FEATURE_COUNT_THD = 0.33


def find_ngram_list(in_list, nr):
    res_dict = dict()
    # cover_dict = dict()
    for idx in range(len(in_list) - nr + 1):
        _key = str(in_list[idx: idx + nr])
        if _key not in res_dict:
            res_dict[_key] = 1
            # cover_dict[_key] = 1
        else:
            res_dict[_key] += 1
    # return res_dict, cover_dict
    return res_dict


def pattern_count_filter(pattern_dict, all_patterns):
    # find the most frequent pattern and add it to the all-cover pattern dict
    pd_list = sorted(pattern_dict.items(), key=lambda x: x[1], reverse=True)
    if pd_list[0][0] not in all_patterns:
        all_patterns[pd_list[0][0]] = 1


def pattern_filter(pattern_dict, final_patterns):
    pd_list = sorted(pattern_dict.items(), key=lambda x: x[1], reverse=True)
    tmp_pattern = list()
    # tmp_pattern = tuple()
    # is_covered = False
    for pat_item in pd_list:
        # add to patterns
        # if pat_item[0] not in final_patterns and len(tmp_pattern) == 0:
        if pat_item[0] not in final_patterns:
            tmp_pattern.append(pat_item)
        elif pat_item[0] in final_patterns:
            # is_covered = True
            break

    # if is_covered is False:
        # final_patterns[tmp_pattern[0]] = 1
    if len(tmp_pattern) > 0:
        for item in tmp_pattern:
            final_patterns[item[0]] = 1


if __name__ == '__main__':
    # get input data ==> raw_data
    # input_file = open(sys.argv[1], 'r', encoding='utf-8-sig')
    data_path = sys.argv[1]
    nr_gram = int(sys.argv[2])
    # nr_top = int(sys.argv[3])

    raw_data = []
    if os.path.exists(data_path):
        for file_name in os.listdir(data_path):
            input_file = open(data_path + '/' + file_name)
            for line in input_file:
                _nr = line.lstrip('\xef\xbb\xbf').rstrip(' \r\n')
                nr = list(map(int, _nr.split(' ')))
                raw_data.append(nr)
            input_file.close()
        print('Data loaded.\n')
    else:
        sys.exit(1)

    len_raw_data = 0
    # all_counter = Counter()
    list_ngram_items = list()
    dict_allgrams = dict()
    cover_allpattern = dict()
    all_ngrams= Counter()
    for item in raw_data:
        len_raw_data += len(item)
        item_ngrams = find_ngram_list(item, nr_gram)
        all_ngrams.update(Counter(item_ngrams))
        # all_counter.update(Counter(cover_ngrams))
        # apattern_count_filter(item_ngrams, cover_allpattern)
        pattern_filter(item_ngrams, dict_allgrams)
        # counter_allgrams.update(Counter(item_ngrams))
        # list_ngram_items.append(item_ngrams)

    # all_ngrams = dict(sorted(dict(counter_allgrams).items(), key=lambda x:x[1], reverse=True))
    # all_ngrams = sorted(dict(counter_allgrams).items(), key=lambda x:x[1], reverse=True)
    # top_ngrams = all_ngrams[:nr_top]

    # output all patterns
    # for key, _dict in all_grams.items():
    out_file = open('./all_'+str(nr_gram)+'gram.out', 'w')
    for key, value in dict(all_ngrams).items():
        out_file.write(str(key)+': '+str(1)+'\n')
    out_file.close()

    cover_file = open('./cover_'+str(nr_gram)+'count.out', 'w')
    # count_thd = int(FEATURE_COUNT_THD * len(raw_data))
    # for k, v in all_counter.items():
    #     if v > count_thd:
    #         cover_file.write(str(k)+': '+str(v)+'\n')
    # cover_file.close()

    for k, v in cover_allpattern.items():
        cover_file.write(str(k) + ': ' + str(v) + '\n')
    cover_file.close()
    print('Completed!\n')
    sys.exit(0)
