#!/usr/bin/env python
from __future__ import division
import ast
import sys
import numpy as np
import sklearn.svm as svm
import libsvm.python.svmutil as svmutil
from sklearn.metrics import roc_curve, auc
import matplotlib.pyplot as plt


LINES = [':', '--', '-.']


# adjust distance to hyperplane
def get_fpr_tpr(label, step, pred):
    thd = 0
    res_dict = dict()
    while thd < 1:
        new_pred = list()
        for i in pred:
            if i < thd:
                new_pred.append(1)
            else:
                new_pred.append(-1)
        fpr, tpr, thresholds = roc_curve(label, new_pred)
        # fpr = np.concatenate((fpr, _fpr))
        # tpr = np.concatenate((tpr, _tpr))
        # _, ssfpr = np.unique(fpr, return_index=True)
        # _, sstpr = np.unique(fpr, return_index=True)
        # fpr = fpr[np.sort(ssfpr)]
        # tpr = tpr[np.sort(sstpr)]
        # fpr.sort()
        # tpr.sort()
        if fpr[1] not in res_dict:
            res_dict[fpr[1]] = tpr[1]
        else:
            if res_dict[fpr[1]] < tpr[1]:
                res_dict[fpr[1]] = tpr[1]
        thd += step

    fpr = [i for i in res_dict.keys()]
    fpr.sort()
    tpr = [res_dict[i] for i in fpr]
    fpr.insert(0, 0)
    fpr.append(1)
    tpr.insert(0, 0)
    tpr.append(1)

    return fpr, tpr


# adjust test data portion
def get_fpr_tpr_portion(t_data, n_data, a_data, nu):
    thd = 0
    step = 0.05
    res_dict = dict()
    len_nd = len(n_data)
    len_ad = len(a_data)
    while thd <= (1-step):
        thd += step
        clf = svm.OneClassSVM(kernel='rbf', nu=nu, gamma='auto')
        clf.fit(t_data)
        pred = clf.predict(n_data[:int(len_nd*thd)] + a_data[:int(len_ad*thd)])
        label_test = [[-1] for _ in range(0, int(len_nd*thd))] \
                     + [[1] for _ in range(0, int(len_ad*thd))]
        fpr, tpr, thresholds = roc_curve(label_test, pred)
        # get result and sort
        if fpr[1] not in res_dict:
            res_dict[fpr[1]] = tpr[1]
        else:
            if res_dict[fpr[1]] < tpr[1]:
                res_dict[fpr[1]] = tpr[1]

    fpr = [i for i in res_dict.keys()]
    fpr.sort()
    tpr = [res_dict[i] for i in fpr]
    fpr.insert(0, 0)
    fpr.append(1)
    tpr.insert(0, 0)
    tpr.append(1)

    return fpr, tpr


# adjust nu to get fpr-tpr
def getnu_fpr_tpr(data_train, data_test, label_test):
    nu_list=[0.5, 0.25, 0.2, 0.1, 0.08, 0.075, 0.05, 0.025,
             0.02, 0.01, 0.008, 0.005, 0.0025, 0.002, 0.001]
    # default nu == 0.5
    clf = svm.OneClassSVM(kernel='rbf')
    clf.fit(data_train)
    # key:fpr, value:tpr
    res_dict = dict()
    # rock and roll!
    # pred = clf.predict(data_test)
    # fpr, tpr, thresholds = roc_curve(label_test, pred)

    for nu in nu_list:
        clf = svm.OneClassSVM(kernel='rbf', nu=nu)
        clf.fit(data_train)

        pred = clf.predict(data_test)
        fpr, tpr, thresholds = roc_curve(label_test, pred)
        if fpr[1] not in res_dict:
            res_dict[fpr[1]] = tpr[1]
        else:
            if res_dict[fpr[1]] < tpr[1]:
                res_dict[fpr[1]] = tpr[1]

        # fpr = np.concatenate((fpr, _fpr))
        # tpr = np.concatenate((tpr, _tpr))
        # _, ssfpr = np.unique(fpr, return_index=True)
        # _, sstpr = np.unique(fpr, return_index=True)
        # fpr = fpr[np.sort(ssfpr)]
        # tpr = tpr[np.sort(sstpr)]
        # fpr.sort()
        # tpr.sort()
    fpr = [i for i in res_dict.keys()]
    fpr.sort()
    tpr = [res_dict[i] for i in fpr]
    fpr.insert(0, 0)
    fpr.append(1)
    tpr.insert(0, 0)
    tpr.append(1)

    return fpr, tpr


if __name__ == '__main__':
    try:
        # m_set is in n1,nu1,n2,nuw,n3,nu3...
        # NU is the \nu parameter
        # e.g., ./plot_roc_ab.py 1,0.05,8,0.01 0.01
        # it will draw ROCs for set1 with \nu=0.05, set2 with \nu=0.01
        # and set {1,8} with \nu=0.01 in one plot
        mu_set = ast.literal_eval('[' + sys.argv[1] + ']')
        NU = float(sys.argv[2])
    except SyntaxError, ValueError:
        print('Get sets/nu failed!')
        sys.exit(1)

    if len(mu_set) % 2 != 0:
        print('Set not complete, check for missing n/nu.')
        sys.exit(1)

    m_set = [mu_set[i] for i in range(0, len(mu_set), 2)]
    nu_dict = {mu_set[i]: mu_set[i+1] for i in range(0, len(mu_set), 2)}

    t_data = list()
    t_dict = dict()
    n_data = list()
    n_dict = dict()
    a_data = list()
    a_dict = dict()
    GAMMA = 1/len(m_set)
    for idx, i in enumerate(m_set):
        cur_dir = './set' + str(i) +'/'
        # read  training
        label_train, _data = svmutil.svm_read_problem(cur_dir + 'svm_train_multi.txt')
        t_dict[i] = _data
        t_data.append([item[1] for item in _data])
        # read validation
        label_normal, _data = svmutil.svm_read_problem(cur_dir + 'svm_validation_multi.txt')
        n_dict[i] = _data
        n_data.append([item[1] for item in _data])
        # read attack
        label_abnormal, _data = svmutil.svm_read_problem(cur_dir + 'svm_attack_multi.txt')
        a_dict[i] = _data
        a_data.append([item[1] for item in _data])

    # transformed into sklearn format
    label_train = [-1 for _ in range(0, len(label_train))]
    label_train = np.array(label_train).reshape(-1, 1)
    label_normal = [-1 for _ in range(0, len(label_normal))]
    label_abnormal = [1 for _ in range(0, len(label_abnormal))]
    label_test = np.array(label_normal + label_abnormal).reshape(-1, 1)

    data_train = map(list, zip(*t_data))
    n_data = map(list, zip(*n_data))
    a_data = map(list, zip(*a_data))
    data_test = n_data + a_data
    # data_test = map(list, zip(*data_test))

    # clf = svm.OneClassSVM(kernel='rbf', nu=NU, gamma=GAMMA)
    clf = svm.OneClassSVM(kernel='rbf', nu=NU, gamma='auto')
    clf.fit(data_train)

    pred = clf.decision_function(data_test)
    # pred = clf.predict(data_test)
    # fpr, tpr, thresholds = roc_curve(label_test, pred)
    fpr, tpr = get_fpr_tpr(label_test, 0.02, pred)
    # fpr, tpr = getnu_fpr_tpr(data_train, data_test, label_test)
    # fpr, tpr = get_fpr_tpr_portion(data_train, n_data, a_data, NU)
    roc_auc = auc(fpr, tpr)

    # if len(m_set) > 1:
    plt_label = 'L-' + str(m_set).replace('[', '{').replace(']', '}') + ' set'
    # else:
    #     plt_label = 'l-' + str(m_set).lstrip('[').rstrip(']') + ' cluster'


    plt.figure(figsize=(8,6))
    plt.plot(fpr, tpr,
             label=plt_label +
                   ' (area = %0.2f)' % roc_auc,
             linewidth=2)
    # plt.plot([0, 1], [0, 1], 'k--')
    for idx, i in enumerate(m_set):
        # train
        _clf = svm.OneClassSVM(kernel='rbf', nu=nu_dict[i], gamma='auto')
        _data_train = [[k[1]] for k in t_dict[i]]
        _clf.fit(_data_train)
        # test
        _n_data = [[k[1]] for k in n_dict[i]]
        _a_data = [[j[1]] for j in a_dict[i]]
        _data_test = _n_data + _a_data
        _pred = _clf.decision_function(_data_test)
        # _pred = _clf.predict(_data_test)
        # _fpr, _tpr, _threshold = roc_curve(label_test, _pred)
        _fpr, _tpr = get_fpr_tpr(label_test, 0.02, _pred)
        # _fpr, _tpr = getnu_fpr_tpr(_data_train, _data_test, label_test)
        # _fpr, _tpr = get_fpr_tpr_portion(_data_train, _n_data, _a_data, NU)
        _roc_auc = auc(_fpr, _tpr)

        plt.plot(_fpr, _tpr,
                 label='L-{0:d} cluster (area = {1:.2f})'.format(i, _roc_auc),
                 linestyle=LINES[idx % len(LINES)],
                 linewidth=2)
    plt.xlim([0.0, 1.0])
    plt.yticks(np.arange(0, 1.01, 0.1))
    plt.xticks(np.arange(0, 1.01, 0.1))
    plt.ylim([0.0, 1.0])
    plt.grid(True, linestyle='-.', color='k')
    plt.minorticks_on()
    plt.xlabel('False Alarm Rate', fontsize=16)
    plt.ylabel('Detection Rate', fontsize=16)
    plt.title('ROC Curves', fontsize=24)
    plt.legend(loc="lower right")
    plt.tight_layout()
    filename= 'roc_'+'_'.join(str(i) for i in m_set)+'.eps'
    # plt.savefig(filename, dpi=800)
    plt.show()
