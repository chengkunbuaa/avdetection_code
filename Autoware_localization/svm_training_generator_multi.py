#!/usr/bin/env python
from __future__ import division
import ast
import sys
import os
import re
import datetime as dt
import libsvm.python.svm as svm
import libsvm.python.svmutil as svmutil
import numpy as np
from sklearn import decomposition as SKD
from sklearn.preprocessing import normalize

# TRAINING = 0.8
# TESTING = 1 - TRAINING
# NR_SEC = 20550
NR_PCA = 0
TRAINING_PATH = './training_data/'


# count overlapping (sliding window)
def occurrences(string, sub):
    count = start = 0
    while True:
        start = string.find(sub, start) + 1
        if start > 0:
            count += 1
        else:
            return count


# return count of how many times a list b appears in list a
# THIS IS SLOW!
# try convert both lists to strings
def count_freq(a, b):
    assert isinstance(a, list)
    assert isinstance(b, list)
    len_a = len(a)
    len_b = len(b)
    count = 0
    if len_a < len_b:
        return count
    for i in range(len_a):
        # reaching end?
        if i + len_b > len_a:
            break
        # flag = False
        matched = 0
        for j in range(len_b):
            if a[i + j] != b[j]:
                break
            else:
                matched += 1
        if matched == len_b:
            count += 1

    return count


def count_freq_str(a, b):
    assert isinstance(a, str)
    assert isinstance(b, str)

    return a.count(b)
    # return occurrences(a, b)


def divide_list(list_a, n):
    # divide list_a into chunks of size N
    for k in range(0, len(list_a), n):
        yield list_a[k:k+n]


def count_patterns(data_list, pattern_list):
    res_data = list()
    for item in data_list:
        cur_count = dict()
        total_covers = 0
        count_all = 0
        if NR_PCA > 0:
            _cur_count = list()
        for pattern in pattern_list:
            # count = count_freq(raw_data, pattern)
            _count = count_freq_str(item, pattern)
            count_all += _count
            if _count > 0:
                total_covers += 1
            if NR_PCA > 0:
                _cur_count.append(_count)
        cur_count[1] = count_all
        # best is total_covers / len(item.rstrip('\n').split(' '))
        #cur_count[2] = total_covers / len(item.rstrip('\n').split(' '))
        cur_count[2] = count_all / len(item.rstrip('\n').split(' '))
        # for key, value in cur_item.items():
        #     # cur_item[key] = int(round((value/total_count), 3) * 1000)
        #     # cur_item[key] = int(round((value / len(item)), 3) * 1000)
        #     cur_item[key] = value / len(item.rstrip('\n').split(' '))
        #     if NR_PCA > 0:
        #         # float need to be cast to string for PCA
        #         _cur_item.append(str(cur_item[key]))
        if NR_PCA > 0:
            res_data.append(_cur_count)
        else:
            res_data.append(cur_count)

    # len_train = len(train_data)
    # len_test = len(train_data) - len_train

    # svm_prob = svm.svm_problem(svm_label[:len_train], svm_data[:len_train])
    # svm_param = svm.svm_parameter('-s 2 -n 0.01 -g 0.1 -t 1')
    # svm_model = svmutil.svm_train(svm_prob, svm_param)

    # PCA analysis
    if NR_PCA > 0:
        pca = SKD.PCA(n_components=NR_PCA)
        # svm_data_array = np.array(svm_data)
        # svm_data_str = map(str, svm_data)

        # scaler = sk.preprocessing.StandardScaler()
        # sd_std = scaler.fit_transform(svm_data)
        # pca_data = pca.fit_transform(sd_std).tolist()
        pca_data = pca.fit_transform(svm_data).tolist()

        # est = KMeans(n_clusters=1)
        # est.fit(_pca_data)
        # centre = est.cluster_centers_

        res_data = list()
        for item in pca_data:
            _cur = dict()
            for key, value in enumerate(item):
                _cur[key + 1] = value
            res_data.append(_cur)

    return res_data


def reduce_results(data_list):
    # reduce all sub lists in data_list to a single one
    reduced_res = list()
    for idxi in range(len(data_list[0])):
        _res_item = list()
        for idxj in range(len(data_list)):
            # only extract covered pattern count
            _res_item.append(data_list[idxj][idxi][2])
        reduced_res.append(_res_item)

    return reduced_res


def normalize_data(data_list):
    # data_list is a 2-d array
    # each item is a 1-d array
    # normalize each item
    normed_data = list()
    for _cur_item in data_list:
        _x = np.array(_cur_item)
        norm = normalize(_x[:, np.newaxis], axis=0).ravel()
        _cur_item = list(norm)
        normed_data.append(_cur_item)

    return normed_data


def get_patterns(dir_path):
    pat_lists = list()
    # read all files and sort by their name
    # assume each file is named like [prefix]idx[suffix]
    # no numbers in pre/suffix
    files = os.listdir(dir_path)
    nr_file_dict = dict()
    nr_file_list = list()
    for file_name in files:
        file_nr = int(re.search('\D*(\d+)\D*', file_name).group(1))
        nr_file_list.append(file_nr)
        nr_file_dict[file_nr] = file_name
    nr_file_list.sort()
    # read files orderly
    for file_nr in nr_file_list:
        file_name = nr_file_dict[file_nr]
        print('pattern file is {}'.format(file_name))
        pattern_file = open(dir_path + '/' + file_name, 'r')
        pat_list_str = list()
        for line in pattern_file:
            # in case we printed all patterns with their values
            line = line.rstrip('\r\n').rstrip('1').rstrip(' ').rstrip(':')
            new_pattern = ast.literal_eval(line)
            # pat_list.append(new_pattern)
            pat_list_str.append(' '.join(str(x) for x in new_pattern))
        # add current pattern file and close it
        pat_lists.append(pat_list_str)
        pattern_file.close()

    return pat_lists


if __name__ == '__main__':
    # alist = [1,3,4,2,4,5,2,3,1,2,3]
    # blist = [2,3]
    #
    # print(count_freq(alist, blist))
    pattern_path = sys.argv[1]
    pat_len = int(sys.argv[2])
    # data_file = open(sys.argv[2], 'r')
    if len(sys.argv) > 3:
        try:
            NR_PCA = int(sys.argv[2])
        except ValueError:
            print('Invalid NR_PCA!')
            sys.exit(1)

    # pat_list is a list containing all found patterns
    pat_lists = get_patterns(pattern_path)
    # pat_len = len(pat_lists[0])

    # read and parse raw data
    train_data = list()
    # raw data will finally be presented as a string
    raw_data_str = ' '
    for file_name in os.listdir(TRAINING_PATH):
        input_file = open(TRAINING_PATH + file_name, 'r')
        # cur_data = list()
        for line in input_file:
            nr = line.lstrip('\xef\xbb\xbf').rstrip(' \r\n')
            # cur_data += map(int, nr.split(' '))
            train_data.append(nr)
        input_file.close()

    # data_file.close()
    print('Data Loaded! Generating SVM training data...\n')

    # prepare svm
    svm_label = [1 for _ in range(int(len(train_data)))]
    svm_data = list()
    # count and output
    # pat_out = open('./pattern_count.out', 'w')

    res_data_list = list()
    for idx, p_list in enumerate(pat_lists):
        # _res_dict = dict()
        _res_data = count_patterns(train_data, p_list)
        res_data_list.append(_res_data)

    # reduce all lists to a single one
    final_res = reduce_results(res_data_list)


    # save data
    if NR_PCA > 0:
        data_save_file = open('./svm_train_multi_pca_' + str(NR_PCA) + '.txt', 'w')
    else:
        data_save_file = open('./svm_train_multi' + str(pat_len) + '.txt', 'w')

    for item in final_res:
        data_save_file.write('+1 ')
        for idx, value in enumerate(item):
            data_save_file.write('{}:{} '.format(idx+1, value))
            # data_save_file.write('{}:{} '.format(idx_j*2+2,
            #                                      res_data_list[idx_j][idx_i][2]))
        data_save_file.write('\n')
    data_save_file.close()

