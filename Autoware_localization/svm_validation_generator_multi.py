#!/usr/bin/env python
from __future__ import division
import sys
import ast
import os
import numpy as np
import svm_training_generator_multi as STGA
import sklearn as sk
from sklearn import decomposition as SKD
from sklearn.preprocessing import normalize
# for observation
from sklearn.cluster import KMeans

NR_PCA = 0
DATA_PATH = './validation_data/'


if __name__ == '__main__':
    # get input data ==> raw_data
    pattern_path = sys.argv[1]
    pat_len = int(sys.argv[2])
    # data_file = open(sys.argv[2], 'r')
    if len(sys.argv) > 3:
        try:
            NR_PCA = int(sys.argv[3])
        except ValueError:
            print('Invalid NR_PCA!')
            sys.exit(1)

    # pat_list is a list containing all found patterns
    # each pattern is a string
    # pat_list = list()
    pat_lists = STGA.get_patterns(pattern_path)
    # for file_name in os.listdir(pattern_path):
    #     pattern_file = open(pattern_path + '/' + file_name, 'r')
    #     pat_list_str = list()
    #     for line in pattern_file:
    #         # in case we printed all patterns with their values
    #         line = line.rstrip('\r\n').rstrip('1').rstrip(' ').rstrip(':')
    #         new_pattern = ast.literal_eval(line)
    #         # pat_list.append(new_pattern)
    #         pat_list_str.append(' '.join(str(x) for x in new_pattern))
    #     # add current pattern file and close it
    #     pat_lists.append(pat_list_str)
    #     pattern_file.close()

    raw_data = list()
    # for i in range(1, 834):
    #     input_file = open('./Training_Data_Master/UTD-'+'{0:0>4}'.format(i)+'.txt', 'r')
    #     cur_data = list()
    #     for line in input_file:
    #         nr = line.lstrip('\xef\xbb\xbf').rstrip(' \r\n')
    #         cur_data += map(int, nr.split(' '))
    #     raw_data += cur_data
    #     input_file.close()

    for file_name in os.listdir(DATA_PATH):
        # get current attack name
        # cur_class = dir_name.split('_')[0]
        # read and parse data in each file
        input_file = open(DATA_PATH + file_name, 'r')
        for line in input_file:
            # cur_data = list()
            nr = line.lstrip('\xef\xbb\xbf').rstrip(' \r\n')
            # cur_data += map(int, nr.split(' '))
            raw_data.append(nr)
        input_file.close()

    print('Data loaded.\n')
    # for x in set(raw_data):
    #     print('{}:{}\n'.format(x, raw_data.count(x)))

    res_data_list = list()
    for idx, p_list in enumerate(pat_lists):
        # _res_dict = dict()
        _res_data = STGA.count_patterns(raw_data, p_list)
        res_data_list.append(_res_data)

    # reduce data
    final_res = STGA.reduce_results(res_data_list)
    # normalize
    # final_res = STGA.normalize_data(final_res)

    # save data
    if NR_PCA > 0:
        data_save_file = open('./svm_validation_multi_pca_' + str(NR_PCA) + '.txt', 'w')
    else:
        data_save_file = open('./svm_validation_multi' + str(pat_len) +'.txt', 'w')

    for item in final_res:
        data_save_file.write('+1 ')
        for idx, value in enumerate(item):
            data_save_file.write('{}:{} '.format(idx+1, value))
            # data_save_file.write('{}:{} '.format(idx_j*2+2,
            #                                      res_data_list[idx_j][idx_i][2]))
        data_save_file.write('\n')
    data_save_file.close()

    # save training data for offline run
    # data_save_file = open('./svm_adfa_train.txt', 'w')
    # for idx_i in range(len(res_data_list[0])):
    #     data_save_file.write('+1 ')
    #     for idx_j in range(len(res_data_list)):
    #         data_save_file.write('{}:{} '.format(idx_j + 1,
    #                                              res_data_list[idx_j][idx_i][2]))
    #         # data_save_file.write('{}:{} '.format(idx_j*2+2,
    #         #                                      res_data_list[idx_j][idx_i][2]))
    #     data_save_file.write('\n')
    # data_save_file.close()
