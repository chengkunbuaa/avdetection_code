#!/bin/bash
set -e
for i in `seq 2 8`
do
    { time ./perf_combinations_ab.py 1 8 $i; } 2>time_comb$i.txt &
done

wait

