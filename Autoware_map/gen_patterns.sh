#!/bin/bash
set -e

for i in `seq 1 ${1}`
do 
    ./all_grams.py training $i
    mkdir set$i
    mv all_${i}gram.out set${i}/
done

rm cover_*.out
