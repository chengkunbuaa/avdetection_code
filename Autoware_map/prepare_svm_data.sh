#!/bin/bash

set -e

./svm_training_generator_multi.py $1 $2 $3
./svm_validation_generator_multi.py $1 $2 $3
./svm_attack_generator_multi.py $1 $2 $3
