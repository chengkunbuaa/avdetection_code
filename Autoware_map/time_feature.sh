#!/bin/bash
set -e

for i in `seq 1 20`
do
    { time ./prepare_svm_data.sh set${i} ${i}; } 2> time_feature$i.txt &
    # mv svm*${i}*.txt set$i
done

wait

for i in `seq 20 -1 1`
do
    mv svm_train_multi$i.txt set$i/svm_train_multi.txt
    mv svm_validation_multi$i.txt set$i/svm_validation_multi.txt
    mv svm_attack_multi$i.txt set$i/svm_attack_multi.txt
done

echo "ALL DONE"
