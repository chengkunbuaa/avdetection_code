#!/usr/bin/env python
from __future__ import division
import sys
from libsvm.python import svmutil

NU = [0.5, 0.2, 0.1, 0.05, 0.01, 0.005, 0.001, 0.0005, 0.0001]

if __name__ == '__main__':
    try:
        set_min = int(sys.argv[1])
        set_max = int(sys.argv[2]) + 1
    except ValueError:
        print("get min/max failed!")
        sys.exit(1)

    res_file = open('./perf_svm_'+str(set_min)+'_'+str(set_max-1)+'.csv', 'w')

    for i in range(set_min, set_max):
        cur_dir = "./set"+str(i)
        res_file.write('{},'.format(i))
        tl, td = svmutil.svm_read_problem(cur_dir+'/svm_train_multi.txt')
        vl, vd = svmutil.svm_read_problem(cur_dir+'/svm_validation_multi.txt')
        al, ad = svmutil.svm_read_problem(cur_dir+'/svm_attack_multi.txt')
        for nu in NU:
            model = svmutil.svm_train(tl, td, '-s 2 -n '+str(nu))
            # get true postives
            label, acc, val = svmutil.svm_predict(vl, vd, model)
            tp = int(acc[0] * len(label) / 100)
            # get true negtives
            label, acc, val = svmutil.svm_predict(al, ad, model)
            tn = int(acc[0] * len(label) / 100)
            res_file.write('{},{},'.format(tp, tn))
        res_file.write('\n')
    res_file.close()
